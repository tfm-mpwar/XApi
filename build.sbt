name := """XApi"""

version := "1.0-SNAPSHOT"

lazy val xbackend = RootProject(file("../XBackend"))

lazy val root = (project in file(".")).enablePlugins(PlayScala).dependsOn(xbackend)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  filters,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.0-RC1" % Test

)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

routesGenerator := InjectedRoutesGenerator

fork in run := false



fork in run := true